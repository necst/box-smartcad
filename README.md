# Automatic Nodes Deployment For Indoor Localization And Occupancy Monitoring Systems #

=============================

Many Smart Building systems, such as indoor localization or occupancy monitoring sys- tems, require the installation of several transmitting and receiving nodes. The quantity and the positioning of these devices heavily affects the accuracy and the total cost of the system, but tools to automate the nodes configuration currently lack. We propose an open-source design tool for the specification of the building floor plan. The tool is able to suggest a near optimal allocation of sensor nodes, de- pending on hardware characteristics and prices, in order to maximize the coverage area while minimizing the total cost.

=============================

## Compilation instructions: ##

### Windows ###

Download and install a C++ compiler and Windows SDK, for example:

Visual Studio C++ Express

Microsoft Windows SDK for Windows 7 and .NET Framework 4

Download and install Qt (see supported platforms):

Download for example the ZIP file called qt-everywhere-opensource-src-4.7.4.zip

Extract the ZIP file

Configure Qt. E.g. on cygwin with:

configure.exe -platform win32-msvc2010 -debug

or:

configure.exe -platform win32-msvc2010 -release

Compile Qt. E.g. on cygwin with nmake or jom:


nmake release

Add the path to the Qt binaries (for example C:\Qt\qt-everywhere-opensource-src-4.7.4\bin) to your PATH environment variable

Set environment variable QMAKESPEC to win32-msvc2010 or the appropriate downloaded version

Configure QCAD. For example on cygwin with:


cd /home/user/qcad

qmake -r

Compile QCAD. For example on cygwin:

nmake release

Alternatively, you may want to use jom, an nmake clone which speeds up compilation significantly by using all available CPU cores:

Launch QCAD. E.g. from cygwin with:

./release/qcad.exe

### Mac OS X ###

Download and install the latest version of XCode for your platform.

Download and install Qt (see supported platforms):

Download for example the tar.gz file called qt-everywhere-opensource-src-4.8.4.tar.gz

Extract the tar.gz file:

tar xfvz qt-everywhere-opensource-src-4.8.4.tar.gz

Configure Qt:

cd qt-everywhere-opensource-src-4.8.4

./configure -fast -opensource -arch x86 -release \

-no-qt3support -qt-zlib -qt-libtiff -qt-libpng -qt-libmng \

-qt-libjpeg -confirm-license

Compile Qt:

make 

Add ~/opt/qt-everywhere-opensource-src-4.8.4/bin to your PATH environment variable:


export PATH=~/opt/qt-everywhere-opensource-src-4.8.4/bin:$PATH

Configure QCAD:

cd ~/qcad

qmake -r

Compile QCAD:

make release

Launch QCAD:

cd release

./QCAD.app/Contents/MacOS/QCAD

### Linux ###

Check that you have the following (including dependencies) installed:

gcc version 4

make

libx11-dev

libxext-dev

libxrender-dev

libglu1-mesa-dev (Ubuntu)

libfreetype6-dev (Ubuntu)

libfontconfig1-dev (Ubuntu)

libssl-dev (Ubuntu)

libdbus-1-dev (Ubuntu)

libsm-dev (Ubuntu)

Mesa-devel (OpenSUSE)

dbus-1-devel (OpenSUSE)

libsm-devel (OpenSUSE)

glu-devel (OpenSUSE)

If you wish to compile / run QCAD 3 32bit on a 64bit Linux installation, you will also need:

ia32-libs

Mesa-32bit (OpenSUSE)

Download and install Qt (see supported platforms):

Download for example the tar.gz file called qt-everywhere-opensource-src-4.8.4.tar.gz

Extract the tar.gz file:

tar xfvz qt-everywhere-opensource-src-4.8.4.tar.gz

Configure Qt:

cd qt-everywhere-opensource-src-4.8.4

./configure -fast -opensource -release -no-qt3support -fontconfig -dbus \

-sm -qt-libpng -qt-libjpeg -qt-libmng -qt-zlib -openssl -opengl desktop \

-xrender -webkit -confirm-license

Compile Qt:

make

Add ~/opt/qt-everywhere-opensource-src-4.8.4/bin to your PATH environment variable:

export PATH=~/opt/qt-everywhere-opensource-src-4.8.4/bin:$PATH

Configure QCAD:

cd ~/qcad
qmake -r
Compile QCAD:

make release
Launch QCAD:

cd release
LD_LIBRARY_PATH=. ./qcad-bin

=============================

## QCAD ##

QCAD is a 2D CAD solution for Windows, Mac OS X and Linux. Its core is developed in C++, based on the Qt tool kit.
QCAD can be extended through a C++ plugin interface as well as through its very powerful and complete scripting 
interface (ECMAScript/JavaScript).

### License ###
-------
The QCAD 3 source code is released under the GPLv3 open source license. Script add-ons and C++ plugins 
are released under their respective licenses.